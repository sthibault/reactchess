import React from "react";
import lodash from "lodash";
import './Game.css';
import * as Chess from "./Piece";
import { K, Q, R, B, N, W, P, Bl, Empty } from "./Piece";


const emptyHighlight = [
    ["", "", "", "", "", "", "", ""],
    ["", "", "", "", "", "", "", ""],
    ["", "", "", "", "", "", "", ""],
    ["", "", "", "", "", "", "", ""],
    ["", "", "", "", "", "", "", ""],
    ["", "", "", "", "", "", "", ""],
    ["", "", "", "", "", "", "", ""],
    ["", "", "", "", "", "", "", ""]
  ];
  
function Square(props) {
  return (
    <button className={props.classes} onClick={ props.onClick }>
      <div className={"piece " + props.state}>{ props.value ? props.value.gliph : null }</div>
    </button>
  );
}

class Board extends React.Component {
  renderSquare(rank, file , dark, mark) {
    return (
      <Square
        classes={ "square " + (dark === true ? "dark" : "light") +
                  (rank === this.props.selectedSquare[0] && file === this.props.selectedSquare[1] ? " square-selected" : "") }
        value={ this.props.squares[rank][file] }
        key={ rank * 8 + file }
        state={ mark }
        onClick={ (e) => this.props.onClick(e, rank, file) }
      />
    );
  }
  
  renderRankLabels(rank) {
    return (<span key={rank} className="board-rank-label">{ 8 - rank }</span>);
  }
  
  renderRank(rank) {
    let squares = [];
    let dark = rank % 2 === 0;
    
    for (let file = 0; file < 8; file++)
    {
      squares.push(this.renderSquare(rank, file, dark, this.props.highlightedSquares[rank][file]));
      dark = !dark;
    }
    
    return (
      <div className="board-rank" key={rank}>
        { this.renderRankLabels(rank) }
        { squares }
      </div>
    )
  }

  renderFileLabels() {
    return (
      <div className="board-files">
        <span className="board-file-label">A</span>
        <span className="board-file-label">B</span>
        <span className="board-file-label">C</span>
        <span className="board-file-label">D</span>
        <span className="board-file-label">E</span>
        <span className="board-file-label">F</span>
        <span className="board-file-label">G</span>
        <span className="board-file-label">H</span>
      </div>
    )
  }
  
  render() {
    return (
      <div>
        {this.renderRank(0)}
        {this.renderRank(1)}
        {this.renderRank(2)}
        {this.renderRank(3)}
        {this.renderRank(4)}
        {this.renderRank(5)}
        {this.renderRank(6)}
        {this.renderRank(7)}
        {this.renderFileLabels()}
      </div>
    );
  }
}

class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedPiece: null,
      selectedSquare: [],
      highlightedSquares: emptyHighlight.map(function(arr) { return arr.slice(); }),
      history: [{
        squares:
        [[R(Bl), N(Bl), B(Bl), Q(Bl), K(Bl), B(Bl), N(Bl), R(Bl)],
         [P(Bl), P(Bl), P(Bl), P(Bl), P(Bl), P(Bl), P(Bl), P(Bl)],
         [Empty, Empty, Empty, Empty, Empty, Empty, Empty, Empty],
         [Empty, Empty, Empty, Empty, Empty, Empty, Empty, Empty],
         [Empty, Empty, Empty, Empty, Empty, Empty, Empty, Empty],
         [Empty, Empty, Empty, Empty, Empty, Empty, Empty, Empty],
         [P(W),  P(W),  P(W),  P(W),  P(W),  P(W),  P(W),  P(W)],
         [R(W),  N(W),  B(W),  Q(W),  K(W),  B(W),  N(W),  R(W)]]}],
      stepNumber: 0,
      turn: "white",
    };
    
    this.printBoard(this.state.history[this.state.stepNumber].squares);
  }
  
  printBoard(board: Array)
  {
    for (let rank = 0; rank < 8; rank++) {
      let pieces = "";
      for (let file = 0; file < 8; file++) {
        pieces += board[rank][file].gliph + ","
      }
      console.log("Rank " + rank + ":" + pieces);
    }
    console.log("end\n");
  }
  
  handleRootClick() {  }
  
  handleSquareClick(e, rank, file) {
    console.log("square Click");
    
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const highlightedSquares = emptyHighlight.map(function(arr) { return arr.slice(); });
    const current = history[history.length - 1];
    let selectedPiece = this.state.selectedPiece;
    let selectedSquare = this.state.selectedSquare;
  
    if (selectedSquare && selectedSquare.length > 0 && selectedSquare[0] === rank && selectedSquare[1] === file) {
      this.setState({
                      highlightedSquares: highlightedSquares,
                      selectedSquare: [],
                      selectedPiece: null,
                    });
      return;
    }
    else {
      selectedSquare = [rank, file];
    }
    
    const squares = current.squares.slice();
    
    console.log(`Step: ${this.state.stepNumber} Length: ${this.state.history.length}`);
    
    // if (selectedPiece && selectedSquare) {
      // A square has already been selected.
      // has another square been selected
        // yes, is that an allowed move?
          // yes, does it have a piece
            // capture
          // no, it doesn't
            // move there

    // }
    // else {
      // no square was selected
      // select it
      // get valid moves
      // set the highlight flag on every piece.
    // }
    
    if (current.squares[rank][file] &&
        Object.getPrototypeOf(current.squares[rank][file].constructor) === Chess.Piece) {
      selectedPiece = current.squares[rank][file].constructor
      console.log("Piece Selected");
      this.printBoard(squares);
  
      selectedPiece = current.squares[rank][file];
      let moves = selectedPiece.getMoves(squares);
      for (const move of moves) {
        let factor = selectedPiece.color === W ? -1 : +1
        let r = rank + (move[0] * factor);
        let f = file + move[1];
        if (r >= 0 && r < 8 && f >=0 && f < 8)
        {
          let p = Object.getPrototypeOf(current.squares[rank][file].constructor);
          if (p === Chess.Piece) {
            if (p.color !== selectedPiece.color) {
              highlightedSquares[r][f] = "captureable";
            }
          }
          else {
            highlightedSquares[r][f] = "captureable";
          }
        }
      }
      
      console.log("Piece moves marked.");
      this.printBoard(squares);
      this.setState({
                      history: history.concat([{
                        squares: squares,
                      }]),
                      stepNumber: history.length,
                      selectedSquare: selectedSquare,
                      selectedPiece: selectedPiece,
                      highlightedSquares: highlightedSquares,
                    });
    }
    
    e.stopPropagation();
  }
  
  jumpTo(step) {
    this.setState({
      stepNumber: step,
      selectedSquare: null,
      selectedPiece: null,
    });
  }
  
  render() {
    this.printBoard(this.state.history[this.state.stepNumber].squares);
    
    const history = this.state.history;
    const current = history[this.state.stepNumber];
    
    const moves = history.map((step, move) => {
      const desc = move ?
        "Go to move #" + move :
        "Go to game start";
      return (
        <li key={ move }>
          <button onClick={ () => this.jumpTo(move) }>
            { desc }
          </button>
        </li>
      )
    });
    
    let status;
    
    status = "Next player: " + (this.state.turn);
    
    return (
      <div className="game"
           onClick={ () => { this.handleRootClick() } } >
        <div className="game-board">
          <Board
            squares={ current.squares }
            highlightedSquares={ this.state.highlightedSquares }
            selectedSquare={ this.state.selectedSquare }
            onClick={ (e, rank, file) => this.handleSquareClick(e, rank, file) }/>
        </div>
        <div className="game-info">
          <div>{ status }</div>
          <ol>{ moves }</ol>
        </div>
      </div>
    );
  }
}

export default Game;
