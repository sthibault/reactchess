const KingChar = "♔";
const QueenChar = "♕";
const RookChar = "♖";
const BishopChar = "♗";
const KnightChar = "♘";
const PawnChar = "♙";

export const Color = Object.freeze({
                 White: { name: "white", value: 0 },
                 Black: { name: "black", value: 6 },
               });

export class Move {
  constructor(piece: Piece, from: number, to: number) {
    this.piece = piece;
    this.from = from;
    this.to = to;
  }
}

export class Piece {
  constructor(piece: string, color: Color, value: number, possibleMoves: Array) {
    this.color = color;
    this.gliph = String.fromCharCode(piece.codePointAt(0) + this.color.value); // assume unicode chess piece
    this.possibleMoves = possibleMoves;
    this.moved = false; // used for pawns, can move two squares, kings/rooks, can castle.
    this.value = value;
  }
  
  getMoves(square: number, board: Array) {
    let moves: Array[] = this.possibleMoves;
    return moves;
  }
  
  canMove(move: Move, lastMove: Move, board: Array) {

  }
  
  move(move: Move, board: Array)
  {
    this.moved = true;
  }
  
  toString() {
    return this.gliph;
  }
}

export class King extends Piece {
  constructor(color: Color) {
    super(KingChar, color, 100000,
          [ /* Rank, File */
            /* North      */ [+1, 0],
            /* South      */ [-1, 0],
            /* East       */ [ 0,+1],
            /* West       */ [ 0,-1],
            /* North East */ [+1,+1],
            /* South West */ [-1,+1],
            /* North East */ [+1,-1],
            /* South West */ [-1,-1]
          ]);
  }
}

export class Queen extends Piece {
  constructor(color: Color) {
    super(QueenChar, color, 9,
          [ /* Rank, File */
            /* North      */ [+1, 0], [+2, 0], [+3, 0], [+4, 0], [+5, 0], [+6, 0], [+7, 0],
            /* South      */ [-1, 0], [-2, 0], [-3, 0], [-4, 0], [-5, 0], [-6, 0], [-7, 0],
            /* East       */ [ 0,+1], [ 0,+2], [ 0,+3], [ 0,+4], [ 0,+5], [ 0,+6], [ 0,+7],
            /* West       */ [ 0,-1], [ 0,-2], [ 0,-3], [ 0,-4], [ 0,-5], [ 0,-6], [ 0,-7],
            /* North East */ [+1,+1], [+2,+2], [+3,+3], [+4,+4], [+5,+5], [+6,+6], [+7,+7],
            /* South East */ [-1,+1], [-2,+2], [-3,+3], [-4,+4], [-5,+5], [-6,+6], [-7,+7],
            /* North West */ [+1,-1], [+2,-2], [+3,-3], [+4,-4], [+5,-5], [+6,-6], [+7,-7],
            /* South West */ [-1,-1], [-2,-2], [-3,-3], [-4,-4], [-5,-5], [-6,-6], [-7,-7],
          ]);
  }
}

export class Rook extends Piece {
  constructor(color: Color) {
    super(RookChar, color, 5,
          [ /* Rank, File */
            /* North */ [+1, 0], [+2, 0], [+3, 0], [+4, 0], [+5, 0], [+6, 0], [+7, 0],
            /* South */ [-1, 0], [-2, 0], [-3, 0], [-4, 0], [-5, 0], [-6, 0], [-7, 0],
            /* West  */ [ 0,+1], [ 0,+2], [ 0,+3], [ 0,+4], [ 0,+5], [ 0,+6], [ 0,+7],
            /* East  */ [ 0,-1], [ 0,-2], [ 0,-3], [ 0,-4], [ 0,-5], [ 0,-6], [ 0,-7],
          ]);
  }
}

export class Bishop extends Piece {
  constructor(color: Color) {
    super(BishopChar, color, 3,
          [ /* Rank, File */
            /* North West */ [+1,+1], [+2,+2], [+3,-3], [+4,+4], [+5,+5], [+6,+6], [+7,+7],
            /* North East */ [+1,-1], [+2,-2], [+3,+3], [+4,-4], [+5,-5], [+6,-6], [+7,-7],
            /* South West */ [-1,-1], [-2,-2], [-3,-3], [-4,-4], [-5,-5], [-6,-6], [-7,-7],
            /* South East */ [-1,+1], [-2,+2], [-3,+3], [-4,+4], [-5,+5], [-6,+6], [-7,+7]
          ]
    );
  }
}

export class Knight extends Piece {
  constructor(color: Color) {
    super(KnightChar, color, 3,
          [ /* Rank, File */
            /* North East */ [+2,+1],
            /* South East */ [+2,-1],
            /* North West */ [-2,+1],
            /* South West */ [-2,-1],
            /* North East */ [+1,+2],
            /* South East */ [+1,-2],
            /* North West */ [-1,+2],
            /* South West */ [-1,-2]
          ]
    );
  }
}

export class Pawn extends Piece {
  constructor(color: Color) {
    super(PawnChar, color, 1,
          [ /* Rank, File */
            [+1,0],[+2,0],[+1,+1],[+1,-1]]);
  }
  
  canEnPassent(move: Move, lastMove: Move) {
    if ((lastMove.piece === PawnChar) &&
        (lastMove.to === move.from + 1 || lastMove.to === move.from -1)){
      return true;
    }
  }
  
  canCapture(move: Move, moveTo: Move, board: Array ) {
    return (board[moveTo] && board[moveTo].color !== this.color);
  }
}

export const W: Color = Color.White;
export const Bl: Color = Color.Black;
export const Empty = "";

export function K(color: Color){
  return new King(color)
}

export function Q(color: Color){
  return new Queen(color);
}

export function R(color: Color) {
  return new Rook(color);
}

export function B(color: Color) {
  return new Bishop(color);
}

export function N(color: Color) {
  return new Knight(color);
}

export function P(color: Color) {
  return new Pawn(color);
}